﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Manning.MyPhotoAlbum;
using Manning.MyPhotoControls;

namespace MyAlbumExplorer
{
    internal class AlbumNode : TreeNode, IDisposable, IRefreshableNode
    {
        private string albumPath;
        private AlbumManager manager;
        public string AlbumPath
        {
            get
            {
                return albumPath;
            }
        }
        public AlbumNode(string name, string albumPath)
            : base(name)
        {
            if (albumPath == null)
                throw new ArgumentNullException("albumPath");
            if (!File.Exists(albumPath))
                throw new ArgumentException(
                "albumPath is not a valid path");
            manager = null;
            albumPath = Path.GetFullPath(albumPath);
            this.Nodes.Add("child");
            if (AlbumStorage.IsEncrypted(albumPath))
            {
                this.ImageKey = "AlbumLock";
                this.SelectedImageKey = "AlbumLock";
            }
            else
            {
                this.ImageKey = "Album";
                this.SelectedImageKey = "AlbumSelect";
            }
        }
        public AlbumManager GetManager(bool interactive)
        {
            if (manager == null)
            {
                string path = AlbumPath;
                string pwd = null;
                try
                {
                    if (AlbumStorage.IsEncrypted(path))
                    {
                        DialogResult result = DialogResult.None;
                        if (interactive)
                        {
                            result = MessageBox.Show("The album "
                            + path + " is encrypted. "
                            + "Do you wish to open this album?",
                            "Encrypted Album",
                            MessageBoxButtons.YesNo,
                            MessageBoxIcon.Question,
                            MessageBoxDefaultButton.Button2);
                        }
                        if (result != DialogResult.Yes
                        || !AlbumController.CheckAlbumPassword(
                        path, ref pwd))
                            return null; // cancelled
                    }
                    manager = new AlbumManager(path, pwd);
                    this.ImageKey = "Album";
                    this.SelectedImageKey = "AlbumSelect";
                }
                catch (AlbumStorageException ex)
                {
                    if (interactive)
                        MessageBox.Show("The album could not "
                        + "be opened [" + ex.Message + "]");
                    this.ImageKey = "AlbumError";
                    this.SelectedImageKey = "AlbumError";
                    manager = null;
                }
            } return manager;
        }
        public PhotoAlbum GetAlbum(bool interactive)
        {
            AlbumManager mgr = GetManager(interactive);
            if (mgr == null) return null;
            else return mgr.Album;
        }
        public void RemoveChildren()
        {
            AlbumManager mgr = GetManager(false);
            if (mgr != null)
            {
                foreach (Photograph p in mgr.Album)
                    p.ReleaseImage();
            }
            Nodes.Clear();
            Nodes.Add("child");
        }
        public void Dispose()
        {
            if (manager != null)
                manager.Album.Dispose();
            manager = null;
        }
        public void RefreshNode()
        {
            AlbumManager mgr = GetManager(false);
            if (mgr != null && this.Text != mgr.ShortName)
                this.Text = mgr.ShortName;
        }
        public bool RenameAlbum(string newName)
        {
            try
            {
                AlbumManager mgr = GetManager(false);
                if (mgr == null)
                    albumPath = AlbumManager.RenameAlbum(
                    AlbumPath, newName);
                else
                {
                    mgr.RenameAlbum(newName);
                    albumPath = mgr.FullName;
                }
                return true;
            }
            catch (ArgumentException)
            {
                MessageBox.Show("Unable to rename album. An "
                + "album with that name already exists.");
                return false;
            }
        }
        public void UpdatePath(string newPath)
        {
            if (!File.Exists(newPath))
                throw new ArgumentException(
                "newPath must be valid path");
            AlbumManager mgr = GetManager(false);
            if (mgr != null)
            {
                // Just pull new info from the manager
                _lbumPath = mgr.FullName;
                Text = mgr.ShortName;
            }
            else
            {
                // use given path to update node
                albumPath = newPath;
                Text = Path.GetFileNameWithoutExtension(
                newPath);
            }
        }
    }
}
public void RenameAlbum(string newName)
{
name = RenameAlbum(FullName, newName);
}
public static string RenameAlbum(
string oldPath, string newName)
{
string dir = Path.GetDirectoryName(oldPath);
string ext = Path.GetExtension(oldPath);
string newPath = dir + Path.
DirectorySeparatorChar + newName + ext;
if (File.Exists(newPath))
{
throw new ArgumentException(
"A file with the name "
+ newPath + " already exists.");

// Presume no error is thrown here
File.Move(oldPath, newPath);
return newPath;
}
}
}
}