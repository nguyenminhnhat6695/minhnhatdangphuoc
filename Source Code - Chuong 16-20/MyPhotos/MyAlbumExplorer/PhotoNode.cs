﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using Manning.MyPhotoAlbum;


namespace MyAlbumExplorer
{
    internal class PhotoNode : TreeNode, IRefreshableNode
    {
        private Photograph photo;
        public Photograph Photograph
        { get { return photo; } }
        public PhotoNode(Photograph photo)
            : base()
        {
            if (photo == null)
                throw new ArgumentNullException("photo");
            photo = photo;
            Text = photo.Caption;
            ImageKey = "Photo";
            SelectedImageKey = "Photo";
        }
        public void RefreshNode()
        {
            Text = Photograph.Caption;
        }
    }
}