﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Xml;
namespace sở_thú_sài_gòn_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
         private void listBox_MouseDown(object sender, MouseEventArgs e)
        {
            ListBox lb = (ListBox)sender;
            int index = lb.IndexFromPoint(e.X, e.Y);
            //Nếu đã chọn đc item
            if (index != -1)
            {
                //Bắt đầu drag item (chỉ cần text. của item đó)
                DragDropEffects effect = lb.DoDragDrop(lb.Items[index].ToString(), DragDropEffects.Move);
            }
        }

        private void listBox_DragEnter(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Move;
        }

        private void listBox_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.StringFormat)) //ktra dữ liệu vào cần sao chép đúng ko
            {
                ListBox lb = (ListBox)sender;
                //listbox.item.contains ktra item đã có trong lst chưa, có trả về true, ko trả về false
                if (!lstDanhSach.Items.Contains(lstThuMoi.SelectedItem.ToString()))
                    lb.Items.Add(e.Data.GetData(DataFormats.Text));
            }
        }

        private void Save(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();

            sfd.Filter = "XML file(*.xml)|*.xml";

            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                XmlTextWriter xtw = new XmlTextWriter(sfd.FileName, null);

                xtw.Formatting = Formatting.Indented;

                xtw.WriteStartDocument();

                xtw.WriteStartElement("Danhsachthu");

                for (int i = 0; i <= lstDanhSach.Items.Count - 1; i++)
                {
                    xtw.WriteElementString("ten", lstDanhSach.Items[i].ToString());
                }

                xtw.WriteEndElement();

                xtw.WriteEndDocument();

                xtw.Flush();

                xtw.Close();

            }
        }

        private void mnuLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog sfd = new OpenFileDialog();

            sfd.Filter = "XML file(*.xml)|*.xml";

            sfd.RestoreDirectory = true;

            if (sfd.ShowDialog() == DialogResult.OK)
            {
                XmlTextReader xtr = new XmlTextReader(sfd.FileName);
                while (xtr.Read())
                    if (xtr.NodeType == XmlNodeType.Element)
                    {
                        if (xtr.LocalName.Equals("ten"))
                            lstThuMoi.Items.Add(xtr.ReadElementString());
                    }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void mnuClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblTime.Text = String.Format("Bây giờ là {0}:{1}:{2} ngày {3} tháng {4} năm {5}",
                DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
        }
    }
}    

